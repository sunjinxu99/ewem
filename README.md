## 平台介绍
##### 首个 Java版 开源一物一码溯源防伪系统，目前二维码应用非常广泛，生产制造、品牌溯源、农产品、移动巡检、快消品。
##### 大企业根据自己的经营特点定制开发。但中小企业很难找到符合自身需求的标准化产品，定制化开发的高成本也让中小企业望而却步。
##### 故开发一套应用一物一码的轮子，欢迎各位 Watch、Star。

## 内置功能(商业版)
<img src="https://images.gitee.com/uploads/images/2021/1031/174553_a2ee73db_1225299.png" alt="内置功能"/>

#### 系统演示(商业版)
* 演示地址：http://49.234.212.77
* 溯源演示地址：http://49.234.212.77/c?c=TBRFEZRB75SQEMJR97
    c=后面字符串为二维码，可在管理端赋码管理菜单获取，或管理端赋码管理扫码体验；

### 开源版，请遵守Apache License2.0 协议，仅限学习交流，禁止商用。

## 应用场景
<img  src="https://img-blog.csdnimg.cn/8bfdf596398e4dcaab744e9506dbd20a.png" alt="应用领域"/>

## 技术选型（你好好看）
* 前端采用Vue、Element UI。
* 后端采用Spring Boot、Spring Security、Redis & Jwt。
* 权限认证使用Jwt，支持多终端认证系统。
* 吃水不忘挖井人，特别感谢：[Ruoyi](https://gitee.com/y_project/RuoYi)。

## 二维码知识
* https://www.qrcode.com
* https://zhuanlan.zhihu.com/p/21463650
* [微信一物一码官方文档](https://developers.weixin.qq.com/doc/offiaccount/Unique_Item_Code/Unique_Item_Code_API_Documentation.html)


## 加开发者微进群
![输入图片说明](https://images.gitee.com/uploads/images/2021/0809/223955_3ef4e39b_1225299.png "合作联系")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1120/192717_15fb3daf_1225299.png "111116.png")

## 交流群

QQ群：200735978
   